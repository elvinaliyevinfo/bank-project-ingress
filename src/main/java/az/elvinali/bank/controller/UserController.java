package az.elvinali.bank.controller;

import az.elvinali.bank.dto.request.UserRequest;
import az.elvinali.bank.dto.response.UserResponse;
import az.elvinali.bank.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @PostMapping
    public ResponseEntity<Void> addUser(@RequestBody @Valid UserRequest userRequest){
        return userService.addUser(userRequest);
    }

}
