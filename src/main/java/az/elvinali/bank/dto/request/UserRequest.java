package az.elvinali.bank.dto.request;

import jakarta.persistence.Column;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class UserRequest {

    @NotEmpty(message = "username can not be empty")
    @NotBlank(message = "username must contain alphabetic characters")
    private String username;

    @NotEmpty(message = "password can not be empty")
    @NotBlank(message = "password must contain alphabetic characters")
    private String password;

}
