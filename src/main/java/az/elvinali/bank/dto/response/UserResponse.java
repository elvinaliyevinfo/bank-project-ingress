package az.elvinali.bank.dto.response;

import az.elvinali.bank.entity.Address;
import jakarta.persistence.*;
import lombok.Data;

@Data
public class UserResponse {

    private Long id;

    private String username;

    private String password;

    private Address address;

}
