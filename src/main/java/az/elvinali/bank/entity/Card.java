package az.elvinali.bank.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "cards")
@Data
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @ToString.Exclude
    private Account account;

    private String cardNumber;
    private String cardType;
    private String expirationDate;

    @OneToMany(mappedBy = "card")
    @EqualsAndHashCode.Exclude
//    @Fetch(FetchMode.SELECT)
    private Set<CardBenefit> benefits;
}