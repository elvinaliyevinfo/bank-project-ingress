package az.elvinali.bank.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Entity
@Table(name = "addresses")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String street;
    private String city;
    private String postalCode;

    @OneToOne
    @JoinColumn(name = "user_id")
    @ToString.Exclude
//    @JsonIgnore
    @EqualsAndHashCode.Exclude
    private User user;
}