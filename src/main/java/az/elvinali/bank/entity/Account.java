package az.elvinali.bank.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.util.List;

@Entity
@Table(name = "accounts")
@Data
@NamedQuery(name = "test", query = "select a from Account a where a.balance>:minBalance")
@NamedEntityGraph(name = "account-user-cards-benefits", attributeNodes = {
        @NamedAttributeNode(value = "user"),
        @NamedAttributeNode(value = "cards", subgraph = "cards.benefits")
}, subgraphs = {
        @NamedSubgraph(name = "cards.benefits", attributeNodes = {
                @NamedAttributeNode(value = "benefits")
        })
})
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
//    @ToString.Exclude
    private User user;

    private String accountNumber;
    private double balance;

    //    @ToString.Exclude
    @OneToMany(mappedBy = "account")
    private List<Card> cards;
}