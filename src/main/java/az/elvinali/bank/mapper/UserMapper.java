package az.elvinali.bank.mapper;

import az.elvinali.bank.dto.request.UserRequest;
import az.elvinali.bank.dto.response.UserResponse;
import az.elvinali.bank.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserResponse mapEntityToResponse(User user);

    User mapRequestToEntity(UserRequest userResponse);

}
