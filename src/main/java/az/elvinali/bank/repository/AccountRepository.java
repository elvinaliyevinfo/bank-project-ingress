package az.elvinali.bank.repository;

import az.elvinali.bank.entity.Account;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account,Long> {
    @Override
    List<Account> findAll();

    @EntityGraph(attributePaths = {"user","cards","cards.benefits","user.address"})
    @Query("select a from Account a")
    List<Account> findAllCustom();


    @Query(name = "test")
    @EntityGraph(attributePaths = {"cards","cards.benefits","user"})
    List<Account> getAccountTest(@Param("minBalance") double balance);

    @EntityGraph(value = "account-user-cards-benefits",type = EntityGraph.EntityGraphType.FETCH)
    @Query("select a from Account a")
    List<Account> customUsingEntityGraphInEntity();

}
