package az.elvinali.bank.service;


import az.elvinali.bank.entity.Student;

import java.util.List;

public interface StudentService {
    Long saveStudent(Student student);

    List<Student> getAllStudents(String name, String surname);

    List<Student> getStudents(String name, String surname, Long age, String gender);
}
