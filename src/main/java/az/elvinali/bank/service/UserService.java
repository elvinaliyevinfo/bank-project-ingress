package az.elvinali.bank.service;

import az.elvinali.bank.dto.request.UserRequest;
import org.springframework.http.ResponseEntity;

public interface UserService {
    ResponseEntity<Void> addUser(UserRequest userRequest);
}
