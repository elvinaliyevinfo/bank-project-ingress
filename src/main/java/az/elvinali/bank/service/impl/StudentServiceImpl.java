package az.elvinali.bank.service.impl;

import az.elvinali.bank.entity.Student;
import az.elvinali.bank.repository.StudentRepository;
import az.elvinali.bank.service.StudentService;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public Long saveStudent(Student student) {
        return studentRepository.save(student).getId();
    }

    @Override
    public List<Student> getAllStudents(String name, String surname) {

        Specification<Student> studentSpecification = new Specification<Student>() {
            @Override
            public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("name"),name);
            }
        };

//        Specification<Student> studentSpecificationLambda =
//        (root,query,criteriaBuilder) -> criteriaBuilder.equal(root.get("name"),name);




//        Specification<Student> studentSpecification = null;

//
//
//        studentSpecification = (root, cq, cb) -> {
//
//            List<Predicate> predicateList = new ArrayList<>();
//
//            if (name != null) {
//                predicateList.add(cb.equal(root.get("name"), name));
//            }
//
//            if (surname != null) {
//                predicateList.add(cb.equal(root.get("surname"), surname));
//            }
//
//            cq.where(cb.and(predicateList.toArray(new Predicate[0])));
//            return cq.getRestriction();
//
//        };
//
//        return studentRepository.findAll(studentSpecification);
        return null;
    }

    @Override
    public List<Student> getStudents(String name, String surname, Long age, String gender) {
        return studentRepository.getStudnets(name, surname, age, gender);
    }

    //elvin

//    private Specification<Student> creSpecification(String name,String surname){
//        Specification<Student> specification=(root,qr,cb) ->{
//            Predicate pred=cb.conjunction();
//            if(name!=null){
//                pred=cb.and(pred,cb.equal(root.get("name"),name));
//            }
//
//            if(surname!=null){
//                pred=cb.or(pred,cb.equal(root.get("surname"),surname));
//            }
//        return pred;
//        };
//        return specification;
//    }
}
