package az.elvinali.bank.service.impl;

import az.elvinali.bank.dto.request.UserRequest;
import az.elvinali.bank.entity.User;
import az.elvinali.bank.mapper.UserMapper;
import az.elvinali.bank.repository.UserRepository;
import az.elvinali.bank.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;

    @Override
    public ResponseEntity<Void> addUser(UserRequest userRequest) {

        User user=userMapper.mapRequestToEntity(userRequest);
        userRepository.save(user);
        return ResponseEntity.status(HttpStatus.CREATED)
                .build();
    }
}
