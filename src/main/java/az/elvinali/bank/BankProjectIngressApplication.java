package az.elvinali.bank;

import az.elvinali.bank.entity.Account;
import az.elvinali.bank.entity.User;
import az.elvinali.bank.repository.AccountRepository;
import az.elvinali.bank.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class BankProjectIngressApplication implements CommandLineRunner {

    private final AccountRepository accountRepository;
    private final UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(BankProjectIngressApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        accountRepository.customUsingEntityGraphInEntity().forEach(System.out::println);
//        accountRepository.findAllCustom().forEach(System.out::println);

//       List<Account> accounts= accountRepository.getAccountTest(600);
//
//       for(Account account: accounts){
//           System.out.println(account);;
//       }

//        userRepository.findAll().forEach(System.out::println);

//       List<User> users= userRepository.findAll();
//        for(User user: users){
//            System.out.println(user.getAddress());
//        }
    }
}
